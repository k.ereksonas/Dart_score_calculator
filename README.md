# Dart score calculator program

This is a simple CLI program used to calculate sets, legs and points of darts game. It is written to count score of a two player game. This program is createad with Python programming language. The code can be found within `Dart_score_calculator.py` file and is distributed under a GNU GPL-3.0 License.
